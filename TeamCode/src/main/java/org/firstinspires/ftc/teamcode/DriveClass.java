package org.firstinspires.ftc.teamcode;

import android.graphics.Color;
import android.util.Log;
import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.SwitchableLight;
import java.io.PrintStream;

public class DriveClass implements Runnable{
    private float PowerForward = 0;
    private float PowerBackward = 0;
    private float PowerXAxis = 0;
    private float PowerTurn = 0;
    private final DcMotor FLMotor;
    private final DcMotor FRMotor;
    private final DcMotor BLMotor;
    private final DcMotor BRMotor;
    private RevColorSensorV3 WobbleSensor;

    private boolean IsActive = true;
    private boolean active1 = false;
    private boolean active2 = false;
    private boolean active3 = false;
    private boolean active4 = false;

    int t = 0;

    public DriveClass(DcMotor FL, DcMotor FR, DcMotor BL, DcMotor BR) //RevColorSensorV3);
    {
        FLMotor = FL;
        FRMotor = FR;
        BLMotor = BL;
        BRMotor = BR;
    }
    public void setParams(float powerForward, float powerBackward, float powerXAxis, float powerTurn)
    {
        PowerForward = powerForward;
        PowerBackward = powerBackward;
        PowerXAxis = powerXAxis;
        PowerTurn = powerTurn;
    }

    public void setEnd(boolean isActive)
    {
        IsActive = isActive;
    }

    private float Calc(int time, boolean isNegative)
    {
        if(!isNegative) {
            return (float) (1f - (Math.pow(0.44f, time)));
        }
        else
        {
            return -(float) (1f - (Math.pow(0.44f, time)));
        }

    }

    public void DriveForward(float power)
    {
        //left is reversed so the value is -x
        FLMotor.setPower(-power);
        FRMotor.setPower(power);
        BLMotor.setPower(-power);
        BRMotor.setPower(power);
    }

    public void DriveBackward(float power)
    {
        //left is reversed so the value is -x
        power = power * -1;
        FLMotor.setPower(-power);
        FRMotor.setPower(power);
        BLMotor.setPower(-power);
        BRMotor.setPower(power);
    }

    public void DriveOnXAxis(float power)
    {
        FLMotor.setPower(power);
        FRMotor.setPower(power);
        BLMotor.setPower(-power);
        BRMotor.setPower(-power);
    }

    public void turn(float power)
    {
        FLMotor.setPower(-power);
        FRMotor.setPower(-power);
        BLMotor.setPower(-power);
        BRMotor.setPower(-power);
    }

    private void Stop()
    {
        PowerForward = 0;
        PowerBackward = 0;
        PowerXAxis = 0;
        PowerTurn = 0;
        FLMotor.setPower(0);
        FRMotor.setPower(0);
        BLMotor.setPower(0);
        BRMotor.setPower(0);
    }

    public void run()
    {

        while(IsActive)
        {
            if(PowerForward > 0 && PowerBackward == 0 && PowerXAxis == 0 && PowerTurn == 0) {
                if(active1)
                {
                    t++;
                }
                else
                {
                    t = 0;
                    active1 = true;
                    active2 = false;
                    active3 = false;
                    active4 = false;
                }
                //DriveForward(Calc(t, false));
                DriveForward(PowerForward);
            }
            else if(PowerForward == 0 && PowerBackward > 0 && PowerXAxis == 0 && PowerTurn == 0) {
                if(active2)
                {
                    t++;
                }
                else
                {
                    t = 0;
                    active1 = false;
                    active2 = true;
                    active3 = false;
                    active4 = false;
                }
                //DriveBackward(Calc(t, false));
                DriveBackward(PowerBackward);
            }
            else if(PowerForward == 0 && PowerBackward == 0 && PowerXAxis != 0 && PowerTurn == 0) {
                if(active3 && PowerXAxis < 0)
                {
                    t = 0;
                    active1 = false;
                    active2 = false;
                    active3 = false;
                    active4 = true;
                }
                else if(active4 && PowerXAxis > 0)
                {
                    t = 0;
                    active1 = false;
                    active2 = false;
                    active3 = true;
                    active4 = false;
                }
                if(active3 || active4)
                {
                    t++;
                }
                if(PowerXAxis > 0) {
                    DriveOnXAxis(Calc(t, false));
                }
                else
                {
                    DriveOnXAxis(Calc(t, true));
                }
            }
            else if(PowerForward == 0 && PowerBackward == 0 && PowerXAxis == 0 && PowerTurn != 0) {
                turn(PowerTurn);
            }
            else
            {
                Stop();
            }
        }
    }
}
